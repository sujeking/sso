import VueRouter from "vue-router"
import Vue from "vue"

Vue.use(VueRouter);

// darshboard

let home_pages = [
    {
        name: "home",
        path: "/home",
        component: () => import("@/views/dashboard/index.vue"),
    }
];
// log

let log_pages = [
    {
        name: "loglist",
        path: "/loglist",
        component: () => import("@/views/log/log_list.vue"),
    },
    {
        name: "logdetail",
        path: "/logdetail",
        component: () => import("@/views/log/log_detail.vue"),
    }
];
// branch

let branch_pages = [
    {
        name: "branchlist",
        path: "/branchlist",
        component: () => import("@/views/branch/branch_list.vue"),
    },
    {
        name: "branchread",
        path: "/branchread",
        component: () => import("@/views/branch/branch_read.vue"),
    },
    {
        name: "branchadd",
        path: "/branchadd",
        component: () => import("@/views/branch/branch_add.vue"),
    },
    {
        name: "branchedit",
        path: "/branchedit",
        component: () => import("@/views/branch/branch_edit.vue"),
    },
    {
        name: "branchapprovaladd",
        path: "/branchapprovaladd",
        component: () => import("@/views/branch/branch_approval_add.vue"),
    },
    {
        name: "branchapprovaledit",
        path: "/branchapprovaledit",
        component: () => import("@/views/branch/branch_approval_edit.vue"),
    }
];
// ip

let ip_pages = [
    {
        name: "iplist",
        path: "/iplist",
        component: () => import("@/views/ip/ip_list.vue"),
    },
    {
        name: "ipread",
        path: "/ipread",
        component: () => import("@/views/ip/ip_read.vue"),
    },
    {
        name: "ipadd",
        path: "/ipadd",
        component: () => import("@/views/ip/ip_add.vue"),
    },
    {
        name: "ipedit",
        path: "/ipedit",
        component: () => import("@/views/ip/ip_edit.vue"),
    },
    {
        name: "ipapprovaladd",
        path: "/ipapprovaladd",
        component: () => import("@/views/ip/ip_approval_add.vue"),
    },
    {
        name: "ipapprovaledit",
        path: "/ipapprovaledit",
        component: () => import("@/views/ip/ip_approval_edit.vue"),
    }
];
// app

let app_pages = [
    {
        name: "applist",
        path: "/applist",
        component: () => import("@/views/app/app_list.vue"),
    }, {
        name: "appread",
        path: "/appread",
        component: () => import("@/views/app/app_read.vue"),
    },
    {
        name: "appadd",
        path: "/appadd",
        component: () => import("@/views/app/app_add.vue"),
    },
    {
        name: "appedit",
        path: "/appedit",
        component: () => import("@/views/app/app_edit.vue"),
    },
    {
        name: "appapprovaladd",
        path: "/appapprovaladd",
        component: () => import("@/views/app/app_approval_add.vue"),
    },
    {
        name: "appapprovaledit",
        path: "/appapprovaledit",
        component: () => import("@/views/app/app_approval_edit.vue"),
    }

];
// role

let role_pages = [
    {
        name: "rolelist",
        path: "/rolelist",
        component: () => import("@/views/role/role_list.vue"),
    }, {
        name: "roleread",
        path: "/roleread",
        component: () => import("@/views/role/role_read.vue"),
    },
    {
        name: "roleadd",
        path: "/roleadd",
        component: () => import("@/views/role/role_add.vue"),
    },
    {
        name: "roleedit",
        path: "/roleedit",
        component: () => import("@/views/role/role_edit.vue"),
    },
    {
        name: "roleapprovaladd",
        path: "/roleapprovaladd",
        component: () => import("@/views/role/role_approval_add.vue"),
    },
    {
        name: "roleapprovaledit",
        path: "/roleapprovaledit",
        component: () => import("@/views/role/role_approval_edit.vue"),
    }
];
// device

let device_pages = [
    {
        name: "devicelist",
        path: "/devicelist",
        component: () => import("@/views/device/device_list.vue"),
    },
    {
        name: "deviceread",
        path: "/deviceread",
        component: () => import("@/views/device/device_read.vue"),
    },
    {
        name: "deviceadd",
        path: "/deviceadd",
        component: () => import("@/views/device/device_add.vue"),
    },
    {
        name: "deviceedit",
        path: "/deviceedit",
        component: () => import("@/views/device/device_edit.vue"),
    },
    {
        name: "deviceapprovaladd",
        path: "/deviceapprovaladd",
        component: () => import("@/views/device/device_approval_add.vue"),
    },
    {
        name: "deviceapprovaledit",
        path: "/deviceapprovaledit",
        component: () => import("@/views/device/device_approval_edit.vue"),
    }
];

// user

let user_pages = [
    {
        name: "userlist",
        path: "/userlist",
        component: () => import("@/views/user/user_list.vue"),
    },
    {
        name: "useradd",
        path: "/useradd",
        component: () => import("@/views/user/user_add.vue"),
    }, 
	{
        name: "useredit",
        path: "/useredit",
        component: () => import("@/views/user/user_edit.vue"),
    },
    {
        name: "userread",
        path: "/userread",
        component: () => import("@/views/user/user_read.vue"),
    },
    {
        name: "userapprovaladd",
        path: "/userapprovaladd",
        component: () => import("@/views/user/user_approval_add.vue"),
    },
    {
        name: "userapprovaledit",
        path: "/userapprovaledit",
        component: () => import("@/views/user/user_approval_edit.vue"),
    }
];


// login && layout

let login = {
    name: "login",
    path: "/login",
    component: () => import("@/views/login/index.vue"),
}
let layout = {
    name: "main",
    path: "/",
    redirect: () => {
        return { path: '/login' }
    },
    component: () => import("@/Layout.vue"),
    children: [
        ...branch_pages,
        ...app_pages,
        ...role_pages,
        ...ip_pages,
        ...device_pages,
        ...log_pages,
        ...user_pages,
        ...home_pages
    ]
}

const router = new VueRouter({
    routes: [
        login,
        layout
    ]
})

router.beforeEach((to, from, next) => {
    next();
})


export default router;
