import Vue from 'vue'
import App from './App.vue'
import router from "./route"
import "./icon.css"

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';


import apptab from "@/components/app_tab.vue";
import appinput from "@/components/app_input.vue";
import appreadinput from "@/components/app_read_input.vue";
import appdateinput from "@/components/app_date_input.vue";
import appselectinput from "@/components/app_select_input.vue";
import apppagetitle from "@/components/app_page_title.vue";
import apptabletitle from "@/components/app_table_title.vue";
import appitemtitle from "@/components/app_item_title.vue";
import appbutton from "@/components/app_button.vue";
import appnavbar from "@/components/app_nav_bar.vue";
import apptenantitem from "@/components/app_tenant_item.vue";
import appsearch from "@/components/app_search.vue"
import appreaditem from "@/components/app_read_item.vue"

Vue.use(ElementUI);
Vue.config.productionTip = false


import VueAwesomeSwiper from "vue-awesome-swiper"
import 'swiper/dist/css/swiper.css'
Vue.use(VueAwesomeSwiper);


Vue.component("apptab", apptab);
Vue.component("appinput", appinput);
Vue.component("appreadinput", appreadinput);
Vue.component("appdateinput", appdateinput);
Vue.component("appselectinput", appselectinput);
Vue.component("apppagetitle", apppagetitle);
Vue.component("apptabletitle", apptabletitle);
Vue.component("appitemtitle", appitemtitle);
Vue.component("appbutton", appbutton);
Vue.component("appnavbar", appnavbar);
Vue.component("appsearch", appsearch);
Vue.component("apptenantitem", apptenantitem);
Vue.component("appreaditem", appreaditem);

// bus
Vue.prototype.$bus = new Vue();
// status color
window.scolors = ["#ff0000", "orange", "#00ff00", "#999999", "#ff0000"];

function confirm(msg, doneHandle, cancelHandle) {
        this.$confirm(msg, 'Warning', {
                confirmButtonText: 'YES',
                cancelButtonText: 'NO',
                type: 'error'
        }).then(doneHandle).catch(cancelHandle);
}
Vue.prototype.showConfirm = confirm



new Vue({
        router,
        render: h => h(App),
}).$mount('#app')
