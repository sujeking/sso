import request from './request.js'

export function loglist(query) {
    return request({
        url: '/api/log',
        method: 'get',
        params: query
    })
}

export function download() {
    return request({
        url: '/api/log/download',
        method: 'get',
    })
}