import request from './request.js'

import qs from "qs"



export function userlist(query) {
    return request({
        url: '/api/user',
        method: 'get',
        params: query
    })
}

export function useradd(query) {
    return request({
        url: '/api/user',
        method: 'post',
        data: qs.stringify(query)
    })
}

export function userdel(query) {
    return request({
        url: '/api/user/' + query,
        method: 'delete',
    })
}
export function usersessiondel(query) {
    return request({
        url: `/api/user/${query}/session`,
        method: 'delete',
    })
}

export function userdetail(query) {
    return request({
        url: '/api/user/' + query,
        method: 'get',
    })
}

export function userupdate(query, data) {
    return request({
        url: '/api/user/' + query,
        method: 'put',
        data: qs.stringify(data)
    })
}


export function userlistpop() {
    return request({
        url: '/api/user/keycloak/index',
        method: 'get',
    })
}





// approval

export function approvaluserlist(query) {
    return request({
        url: '/api/user/approval/index',
        method: 'get',
        params: query
    })
}

export function userpass(id) {
    return request({
        url: `/api/user/${id}/approval/pass`,
        method: 'post',
    })
}

export function userreject(id) {
    return request({
        url: `/api/user/${id}/approval/reject`,
        method: 'post',
    })
}

