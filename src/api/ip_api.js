import request from './request.js'
import qs from "qs"




export function iplist(query) {
    return request({
        url: '/api/ip',
        method: 'get',
        params: query
    })
}

export function ipdetail(query) {
    return request({
        url: '/api/ip/' + query,
        method: 'get',
    })
}

export function ipadd(query) {
    return request({
        url: '/api/ip',
        method: 'post',
        data: qs.stringify(query)
    })
}
export function ipdel(query) {
    return request({
        url: '/api/ip/' + query,
        method: 'delete',
    })
}

export function ipupdate(query, data) {
    return request({
        url: '/api/ip/' + query,
        method: 'put',
        data: qs.stringify(data)
    })
}




// approval

export function approvaliplist(query) {
    return request({
        url: '/api/ip/approval/index',
        method: 'get',
        params: query
    })
}

export function ippass(id) {
    return request({
        url: `/api/ip/${id}/approval/pass`,
        method: 'post',
    })
}

export function ipreject(id) {
    return request({
        url: `/api/ip/${id}/approval/reject`,
        method: 'post',
    })
}

