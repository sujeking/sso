import request from './request.js'
import qs from "qs"

export function branchlist(query) {
        return request({
                url: '/api/branch',
                method: 'get',
                params: query
        })
}

export function branchadd(query) {
        return request({
                url: '/api/branch',
                method: 'post',
                data: qs.stringify(query)
        })
}
export function branchdel(query) {
        return request({
                url: '/api/branch/' + query,
                method: 'delete',
        })
}

export function branchdetail(query) {
        return request({
                url: '/api/branch/' + query,
                method: 'get',
        })
}

export function branchupdate(query, data) {
        return request({
                url: '/api/branch/' + query,
                method: 'put',
                data: data
        })
}


// approval

export function approvalbranchlist(query) {
        return request({
                url: '/api/branch/approval/index',
                method: 'get',
                params: query
        })
}

export function branchpass(id) {
        return request({
                url: `/api/branch/${id}/approval/pass`,
                method: 'post',
        })
}

export function branchreject(id) {
        return request({
                url: `/api/branch/${id}/approval/reject`,
                method: 'post',
        })
}

