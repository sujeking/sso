import request from './request.js'
import qs from "qs"




export function rolelist(query) {
    console.log('====================================');
    console.log(query);
    console.log('====================================');
    return request({
        url: '/api/role',
        method: 'get',
        params: query
    })
}

export function roleadd(query) {
    return request({
        url: '/api/role',
        method: 'post',
        data: qs.stringify(query)
    })
}

export function roledel(query) {
    return request({
        url: '/api/role/' + query,
        method: 'delete',
    })
}

export function roledetail(query) {
    return request({
        url: '/api/role/' + query,
        method: 'get',
    })
}

export function roleupdate(query, data) {
    return request({
        url: '/api/role/' + query,
        method: 'put',
        data: qs.stringify(data)
    })
}



// approval

export function approvalrolelist(query) {
    return request({
        url: '/api/role/approval/index',
        method: 'get',
        params: query
    })
}

export function rolepass(id) {
    return request({
        url: `/api/role/${id}/approval/pass`,
        method: 'post',
    })
}

export function rolereject(id) {
    return request({
        url: `/api/role/${id}/approval/reject`,
        method: 'post',
    })
}
