import request from './request.js'
import qs from "qs"




export function devicelist(query) {
    return request({
        url: '/api/device',
        method: 'get',
        params: query
    })
}

export function deviceadd(query) {
    return request({
        url: '/api/device',
        method: 'post',
        data: qs.stringify(query)
    })
}

export function devicedel(query) {
    return request({
        url: '/api/device/' + query,
        method: 'delete',
    })
}

export function devicedetail(query) {
    return request({
        url: '/api/device/' + query,
        method: 'get',
    })
}

export function deviceupdate(query, data) {
    return request({
        url: '/api/device/' + query,
        method: 'put',
        data: qs.stringify(data)
    })
}


// approval

export function approvaldevicelist(query) {
    return request({
        url: '/api/device/approval/index',
        method: 'get',
        params: query
    })
}

export function devicepass(id) {
    return request({
        url: `/api/device/${id}/approval/pass`,
        method: 'post',
    })
}

export function devicereject(id) {
    return request({
        url: `/api/device/${id}/approval/reject`,
        method: 'post',
    })
}

