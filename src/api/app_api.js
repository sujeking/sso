import request from './request.js'
import qs from "qs"


export function tenantactive(query) {
    return request({
        url: '/api/tenant/session/infos',
        method: 'get',
        params: query
    })
}

export function tenantlist(query) {
    return request({
        url: '/api/tenant',
        method: 'get',
        params: query
    })
}

export function tenantadd(query) {
    return request({
        url: '/api/tenant',
        method: 'post',
        data: qs.stringify(query)
    })
}

export function tenantdel(query) {
    return request({
        url: '/api/tenant/' + query,
        method: 'delete',
    })
}

export function tenantdetail(query) {
    return request({
        url: '/api/tenant/' + query,
        method: 'get',
    })
}

export function tenantupdate(query, data) {
    return request({
        url: '/api/tenant/' + query,
        method: 'put',
        data: qs.stringify(data)
    })
}


// approval

export function approvaltenantlist(query) {
    return request({
        url: '/api/tenant/approval/index',
        method: 'get',
        params: query
    })
}

export function tenantpass(id) {
    return request({
        url: `/api/tenant/${id}/approval/pass`,
        method: 'post',
    })
}

export function tenantreject(id) {
    return request({
        url: `/api/tenant/${id}/approval/reject`,
        method: 'post',
    })
}

