import request from './request.js'


export function leftChartData(dateKey) {
    return request({
        url: `/api/log/stat/login?period=${dateKey}`,
        method: 'get',
    })
}

export function rightChartData(dateKey) {
    return request({
        url: `/api/log/stat/exception?period=${dateKey}`,
        method: 'get',
    })
}
