//
//  AppDelegate.h
//  ios
//
//  Created by sujeking on 2022/2/26.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;
@end

