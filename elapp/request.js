let axios = require("axios").default
// import store from '@/store';
// import { getToken } from '@/utils/auth';
var BASEURL = "https://ssoapi.fornewtech.com";
// create an axios instance
const service = axios.create({
        baseURL: BASEURL, // url = base url + request url
        // withCredentials: true, // send cookies when cross-domain requests
        timeout: 60000 // request timeout
});

// request interceptor
service.interceptors.request.use(
        config => {
                // if (store.getters.token) {
                //     // let each request carry token
                //     // ['X-Token'] is a custom headers key
                //     // please modify it according to the actual situation
                //     config.headers['Authorization'] = getToken();
                // }
                // config.headers["Content-Type"] = 'application/x-www-form-urlencoded'
                console.log("==========================");
                console.log("开始");
                console.log("==========================");
                return config;
        },
        error => {
                console.log(error); // for debug
                return Promise.reject(error);
        }
);

// response interceptor
service.interceptors.response.use(
        response => {
                const res = response.data;

                if (typeof res != "object") {
                        // response.type="arraybuffer"
                        console.log("OK file");
                        return res;
                }
                if (res.statusCode == 200) {
                        return res.data;
                } else {
                        // if (res.resultCode == '401') {
                        //     store.dispatch('user/resetToken').then(() => {
                        //         location.reload();
                        //     });
                        // }
                        // Message({
                        //         message: res.message || 'Error',
                        //         type: 'error',
                        //         duration: 5 * 1000
                        // });

                        // return Promise.reject(new Error(res.message || 'Error'));
                        return Promise.reject(res);

                }
        },
        error => {
                console.log('错误' + error); // for debug
                // if (error.response.status == 401) {
                //     store.dispatch('user/resetToken').then(() => {
                //         location.reload();
                //     });
                //     return;
                // }
                // Message({
                //         message: error.message,
                //         type: 'error',
                //         duration: 5 * 1000
                // });
                return Promise.reject(error);
        }
);

module.exports = service.request;
