// szw test
// let window = {};
// end

window.onload = function () {
        require("machine-uuid")(function (uuid) {
                window.macuuid = uuid;
                let h2 = document.getElementById("macuuid");
                if (h2 != undefined) {
                        h2.innerText = `Device ID ${uuid}`;
                }

                let h3 = document.getElementById("localip");
                if (h3 != undefined) {
                        h3.innerText = `Local IP ${getIPAdress()}`;
                }

                let h4 = document.getElementById("publicip");
                if (h4 != undefined) {
                        h4.innerText = `Public IP deving`;
                }
        })

}



function getIPAdress() {
        var interfaces = require('os').networkInterfaces();
        console.log(interfaces);
        for (var devName in interfaces) {
                var iface = interfaces[devName];
                for (var i = 0; i < iface.length; i++) {
                        var alias = iface[i];
                        if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal) {
                                return alias.address;
                        }
                }
        }
}


// net request
let request = require("./request");
window.request = request;


// downloadfile
window.downloadFile = function (content, fileName, callBack) {
        let fs = require("fs");
        let os = require("os")
        console.log(os.type());
        let type = os.type();
        let path = "";
        if (type == "Darwin") {
                path = process.env.HOME + "/Downloads/" + fileName;
        } else {
                path = process.env.USERPROFILE + "/Downloads/" + fileName;
        }

        fs.writeFile(path, content, callBack);
}
